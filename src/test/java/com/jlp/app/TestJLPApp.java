package com.jlp.app;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestJLPApp {

	WebDriver driver;
	String expectedTitle;
	String actualTitle;

	@BeforeClass
	public void setUp() {
		// code that will be invoked when this test is instantiated
		System.setProperty("webdriver.chrome.driver", "D:/Softwares/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Test
	public void testTitle() {
		try {

			// comment the above 2 lines and uncomment below 2 lines to use Chrome
			// System.setProperty("webdriver.chrome.driver","G:\\chromedriver.exe");
			// WebDriver driver = new ChromeDriver();
			System.setProperty("webdriver.chrome.driver", "D:/Softwares/chromedriver.exe");
			driver = new ChromeDriver();

			String baseUrl = "http://localhost:8082/JLPApp/";
			expectedTitle = "Welcome";
			actualTitle = "";

			// launch Fire fox and direct it to the Base URL
			driver.get(baseUrl);

			// get the actual value of the title
			actualTitle = driver.getTitle();

			/*
			 * compare the actual title of the page with the expected one and print the
			 * result as "Passed" or "Failed"
			 */

			if (actualTitle.contentEquals(expectedTitle)) {
				System.out.println("Test Passed!");
			} else {
				System.out.println("Test Failed");
			}

			expectedTitle = "WelcomePartner";
			actualTitle = driver.getTitle();
			if (actualTitle.contentEquals(expectedTitle)) {
				System.out.println("Test Passed!");
			} else {
				System.out.println("Test Failed");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testLogin() {

		try {
			driver.findElement(By.linkText("Login")).click();
			expectedTitle = "Login";
			actualTitle = driver.getTitle();
			if (actualTitle.contentEquals(expectedTitle)) {
				Assert.assertEquals(actualTitle, expectedTitle);
				System.out.println("Test Passed!");
			} else {
				System.out.println("Test Failed");
			}
			driver.findElement(By.id("username")).clear();
			driver.findElement(By.id("username")).sendKeys("dahveed");
			Thread.sleep(2000);

			// Clear pass and enter password, click submit
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys("123456");
			driver.findElement(By.id("login")).click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testWelcomePage() {
		expectedTitle = "WelcomePartner";
		actualTitle = driver.getTitle();
		if (actualTitle.contentEquals(expectedTitle)) {
			System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed");
		}
	}

	@AfterClass
	public void exit() {
		driver.close();
	}

}
